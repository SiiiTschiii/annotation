package annotation

import (
	"context"
	"errors"
	"fmt"
	"github.com/asticode/go-astisub"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/s3helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

// Caption is the interface from different sources to the Annotation type
type Caption interface {
	Annotations(src string, playlist Playlist) []*Annotation
}

// VideoSuffix contains all sopported media file extensions (e.g. ".mp4")
var VideoSuffix = []string{".mp4", ".mkv", ".m3u8", ".mpd"}

// SubSuffix contains all supported subtitle file extensions (e.g. ".srt")
var SubSuffix = []string{".vtt", ".srt", ".ssa", ".stl", ".ttml"}

// Annotation represents one annotation object.
// An annotation is a subtitle that belongs to a video/movie specified by the src
type Annotation struct {
	ID       primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"` // Unique document _id.
	Subtitle Subtitle           `bson:"subtitle" json:"subtitle"`
	Src      string             `bson:"src" json:"src"` // Reference to the media object this annotation belongs to.
	Usage    []Usage            `bson:"usage,omitempty" json:"usage,omitempty"`
	Playlist Playlist           `bson:"playlist,omitempty" json:"playlist,omitempty"`
}

// Subtitle contains the speech that is spoken in the video between Start- and End time in milliseconds.
// Count is the occurence number of this subtitle. 1 -> first subtitle in the video
type Subtitle struct {
	Count    int         `bson:"count" json:"count"`
	Start    int         `bson:"start" json:"start"`
	End      int         `bson:"end" json:"end"`
	Text     string      `bson:"text" json:"text"`
	Segments []TimedText `bson:"segments,omitempty" json:"segments,omitempty"`
}

// Usage is a type that describes usage characteristics of an annotation
type Usage struct {
	Shared int    `bson:"shared,omitempty" json:"shared,omitempty"`
	Text   string `bson:"text,omitempty" json:"text,omitempty"`
}

// TimedText specifies a text together with a start- and end-time in milliseconds
type TimedText struct {
	Start int    `bson:"start" json:"start"`
	End   int    `bson:"end" json:"end"`
	Text  string `bson:"text" json:"text"`
}

// Playlist specifies the playlist this annotation belongs to
type Playlist struct {
	ID   string `bson:"id" json:"id"`
	Name string `bson:"name" json:"name"`
}

func (s *Subtitle) texts() (texts []string) {

	for i := range s.Segments {
		texts = append(texts, s.Segments[i].Text)
	}

	return texts
}

// FindInSegments tries to find the segment sequence that represents the given text when concatenating it with space as delimiter.
// Returns an error when annoation doesn't have segments or text can't be founct
func (s *Subtitle) FindInSegments(text string) (startIdx int, endIdx int, err error) {

	if s.Segments == nil || len(s.Segments) == 0 {
		return 0, 0, errors.New("subtitle has no segments")
	}

	for startIdx = range s.Segments {
		if strings.HasPrefix(text, s.Segments[startIdx].Text) {
			for endIdx = startIdx + 1; endIdx <= len(s.Segments); endIdx++ {

				texts := s.texts()[startIdx:endIdx]
				if text == strings.Join(texts, " ") {
					return startIdx, endIdx - 1, nil
				}
			}
		}
	}

	return 0, 0, errors.New("text not found")
}

// SubToAnnotation turns astisub.Subtitles into annotations with the src set as passed
func SubToAnnotation(subtitles *astisub.Subtitles, src string, playlist Playlist) (annotations []*Annotation) {
	for i, sub := range subtitles.Items {
		annotation := &Annotation{

			Src: src,
			ID:  primitive.NewObjectID(),
			Subtitle: Subtitle{
				Count: i,
				Start: int(sub.StartAt.Nanoseconds() / 1e6),
				End:   int(sub.EndAt.Nanoseconds() / 1e6),
				Text:  sub.String(),
			},
			Playlist: playlist,
		}
		annotations = append(annotations, annotation)
	}
	return annotations
}

// UploadToDB loads the given annotations into the specified mongoDB collection.
func UploadToDB(annotations []*Annotation, collection *mongo.Collection) error {

	var annoIF []interface{}

	// turn into array of interfaces
	for _, a := range annotations {

		annoIF = append(annoIF, a)
	}

	results, err := collection.InsertMany(context.Background(), annoIF)

	if err != nil {
		return err
	}
	log.Trace("Inserted multiple documents: ", results.InsertedIDs)
	return nil
}

// ConnectMongoDB connects to the mongoDB
// TODO when getting a net.DNSError it is because of the mongodb+srv dns records. Workaround -> configure 8.8.8.8 as dns resolver
func ConnectMongoDB(host string) (*mongo.Client, error) {

	if host == "" {
		return nil, errors.New("mongodb host is empty")
	}

	var mongoURI string

	// check for remote mongoDB with Atlas
	if strings.Contains(host, ".mongodb.net") {

		if os.Getenv("MONGO_ATLAS_USER") == "" {
			return nil, errors.New("env variabl MONGO_ATLAS_USER is not set")
		}

		if os.Getenv("MONGO_ATLAS_PW") == "" {
			return nil, errors.New("env variable MONGO_ATLAS_PW is not set")
		}

		// Fails with mongo-driver > v1.0.0
		mongoURI = fmt.Sprintf("mongodb+srv://%s:%s@%s/test", url.QueryEscape(os.Getenv("MONGO_ATLAS_USER")), url.QueryEscape(os.Getenv("MONGO_ATLAS_PW")), host)

	} else {
		mongoURI = fmt.Sprintf("mongodb://%s:27017", host)
	}

	log.Debug("connection string is: ", mongoURI)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURI))
	if err != nil {
		return nil, err
	}

	ctx, _ = context.WithTimeout(context.Background(), 2*time.Second)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}

	return client, nil
}

// ConnectMongoDBCollection connects to the mongoDB and returns the collection handler
func ConnectMongoDBCollection(host string, db string, collection string) (*mongo.Collection, error) {

	client, err := ConnectMongoDB(host)
	if err != nil {
		return nil, err
	}

	// select/create db
	database := client.Database(db)
	if database == nil {
		return nil, errors.New("client.Database(db) returned nil")
	}

	// select/create collection
	return database.Collection(collection), nil
}

// CreateSub returns the subtitles in the specified time range and adjusts the start and end time to this new range.
// When start time of the first subtitle is below start -> subtitle is included and starttime set to start.
// When end time of the last subtitle is above end -> subtitle is included and endtime set to end.
func CreateSub(source astisub.Subtitles, start time.Duration, end time.Duration) *astisub.Subtitles {
	target := astisub.NewSubtitles()
	// filter subtitle of outside of time range
	for _, sub := range source.Items {
		if sub.StartAt >= start && sub.EndAt <= end {

			target.Items = append(target.Items, sub)
		} else if sub.StartAt < start && sub.EndAt >= start {

			// set subs that span over start to start
			sub.StartAt = start
			target.Items = append(target.Items, sub)
		} else if sub.StartAt < end && sub.EndAt > end {

			// subtitle that span over end to end
			sub.EndAt = end
			target.Items = append(target.Items, sub)
		}

	}
	// adjust start- and endtime
	target.Add(-start)

	return target
}

/*
LoadS3Annotations loads subtitles for media objects on s3 and creates annotations based on them in the specified mongo db collection.
Subtitle files must have supported file extension and a media object must exist in the same directory with the same name and a supported media file extension.
Requires aws-cli. The env variables AWS_CLI, MONGO_PORT, MONGO_HOST, MONGO_DB_NAME  to be set to the aws-cli binary.
Requires the following file structure on s3 when s3PrefixSub = hls/TheBigBangTheory:

├── TheBigBangTheory
	├── The.Big.Bang.Theory.S01E01.mp4
	├── The.Big.Bang.Theory.S01E01.srt

*/
func LoadS3Annotations(workDir string, s3Region string, s3Bucket string, s3PrefixSub string, collection *mongo.Collection, playlist Playlist, expectedCount int) error {

	// only refill db if required
	num, err := collection.CountDocuments(context.Background(), bson.D{})
	if num == int64(expectedCount) {
		log.Infof("No need to refill the annotation db %s: ", collection.Name())
		return nil
	}

	// Empty collection before filling
	err = collection.Drop(context.Background())
	if err != nil {
		return err
	}

	s := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String(s3Region)},
	}))

	objList, err := s3helper.ListS3Obj(s, s3Bucket, s3PrefixSub)
	if err != nil {
		return err
	}

	subList := s3helper.FindSubs(objList, SubSuffix)
	if err != nil {
		return err
	}

	smo := s3helper.VideoExists(objList, subList, VideoSuffix)

	for _, pair := range smo {

		// Download subtitle file from s3
		f, err := s3helper.DownloadS3Obj(s, s3Bucket, *pair.Sub.Key, workDir)
		if err != nil {
			return err
		}

		subs, err := astisub.OpenFile(filepath.Clean(f))
		if err != nil {
			return err
		}

		var src string
		if filepath.Ext(*pair.Media.Key) == ".m3u8" || filepath.Ext(*pair.Media.Key) == ".mpd" {
			// hls streaming manifest
			url := "https://%s.s3.%s.amazonaws.com/%s"
			src = fmt.Sprintf(url, s3Bucket, s3Region, *pair.Media.Key)
		} else {
			src = "s3:///" + *pair.Media.Key
		}

		err = UploadToDB(SubToAnnotation(subs, src, playlist), collection)
		if err != nil {
			return err
		}

		err = os.Remove(f)
		if err != nil {
			return err
		}
	}

	return nil
}

/*
LoadS3AnnotationsStream loads subtitles for hls media streams on s3 and creates annotations based on them in the specified mongo db collection.
Subtitle files must have supported file extension and a video streaming manifest must exist in the same directory with the same name and a supported streaming manifest extension.
Requires the following file structure on s3 when s3PrefixSub = hls/TheBigBangTheory:

├── hls/TheBigBangTheory
	├── The.Big.Bang.Theory.S01E01
		├── master.m3u8
		├── master.srt
	├── The.Big.Bang.Theory.S01E01
*/
func LoadS3AnnotationsStream(workDir string, s3Region string, s3Bucket string, s3PrefixSub string, collection *mongo.Collection, playlist Playlist) error {

	session := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String(s3Region)},
	}))

	args := []string{
		"s3", "ls", "s3://" + filepath.Join(s3Bucket, s3PrefixSub) + "/",
	}
	log.Info(args)
	cmd := exec.Command(os.Getenv("AWS_CLI"), args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return err
	}
	arrOut := strings.Split(string(out), "\n")

	for i, line := range arrOut {
		line = strings.Replace(line, "PRE ", "", 1)
		arrOut[i] = strings.Replace(line, " ", "", -1)
	}
	log.Trace(len(arrOut))

	var matches []*s3helper.SubMediaObj
	for _, folder := range arrOut {
		objList, err := s3helper.ListS3Obj(session, s3Bucket, filepath.Join(s3PrefixSub, folder))
		if err != nil {
			return err
		}

		subList := s3helper.FindSubs(objList, SubSuffix)

		matches = append(matches, s3helper.VideoExists(objList, subList, []string{".mp4", ".mkv", ".m3u8"})...)
	}

	for _, pair := range matches {

		// Download subtitle file from s3
		f, err := s3helper.DownloadS3Obj(session, s3Bucket, *pair.Sub.Key, workDir)
		if err != nil {
			return err
		}

		subs, err := astisub.OpenFile(filepath.Clean(f))
		if err != nil {
			return err
		}

		var src string
		if filepath.Ext(*pair.Media.Key) == ".m3u8" || filepath.Ext(*pair.Media.Key) == ".mpd" {
			// hls streaming manifest
			url := "https://%s.s3.%s.amazonaws.com/%s"
			src = fmt.Sprintf(url, s3Bucket, s3Region, *pair.Media.Key)
		} else {
			src = "s3:///" + *pair.Media.Key
		}

		err = UploadToDB(SubToAnnotation(subs, src, playlist), collection)
		if err != nil {
			return err
		}

		err = os.Remove(f)
		if err != nil {
			return err
		}
	}

	return nil
}

// EmptyMongoCollection drops the mongoCollection
func EmptyMongoCollection(mongoCollection string) error {

	collection, err := ConnectMongoDBCollection(os.Getenv("MONGO_HOST"), os.Getenv("MONGO_DB_NAME"), mongoCollection)
	if err != nil {
		return err
	}

	// Empty collection before filling
	err = collection.Drop(context.Background())
	if err != nil {
		return err
	}

	collection.Database().Client().Disconnect(context.Background())

	return nil
}

// FindLocalSubs finds all subtitles in the passed directory and turns them into annotations
// with the src set to the filepath
func FindLocalSubs(srcDir string, playlist Playlist) (subtitles []*Annotation, err error) {

	err = filepath.Walk(srcDir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.IsDir() {
				for _, suffix := range SubSuffix {
					if filepath.Ext(path) == suffix {
						subs, err := astisub.OpenFile(filepath.Clean(path))
						if err != nil {
							return err
						}
						subtitles = append(subtitles, SubToAnnotation(subs, path, playlist)...)
					}
				}

			}
			return nil
		})
	if err != nil {
		return nil, err
	}
	return subtitles, nil
}
