# annotation

[![pipeline status](https://gitlab.ethz.ch/chgerber/annotation/badges/master/pipeline.svg)](https://gitlab.ethz.ch/chgerber/annotation/commits/master)
[![coverage report](https://gitlab.ethz.ch/chgerber/annotation/badges/master/coverage.svg)](https://gitlab.ethz.ch/chgerber/annotation/commits/master)

Load Subtitles from AWS S3 and YouTube and create Annotation mongoDB collections.

#### Requirements
Environment Variables
```
MONGO_ATLAS_PW
MONGO_ATLAS_USER
MONGO_ATLAS_HOST
MONGO_HOST
MONGO_DB_NAME
```

#### Bugs/Errors
##### DNS error
```bash
panic: error parsing uri (mongodb+srv://$user:$password@composey-y6t4l.mongodb.net/test): lookup composey-y6t4l.mongodb.net on 127.0.0.53:53: cannot unmarshal DNS message
```
###### Reason 
Your DNS resolver can't handle `mongodb+srv://`. 
###### Solution
Add `nameserver 8.8.8.8` as first line to `/etc/resolv.conf`