package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/annotation/v2"
)

func main() {

	log.SetLevel(log.DebugLevel)

	// connect to db
	c, err := annotation.ConnectMongoDBCollection("composey-y6t4l.mongodb.net", "dev", "playlists")
	if err != nil {
		log.Fatal(err)
	}

	//err = youtubedl.LoadYouTubeAnnotationsPlaylist("PLIcq6QAlz4MQfO7Q6NlSd0oKqdSOlj2zI", "en", c, annotation.Playlist{ID: "PLIcq6QAlz4MQfO7Q6NlSd0oKqdSOlj2zI", Name: "Donald Trump"})
	//if err != nil {
	//	log.Fatal(err)
	//}
	//
	//err = youtubedl.LoadYouTubeAnnotationsPlaylist("PLIcq6QAlz4MSiXPMMV9q3Itd0lChE3f-T", "en", c, annotation.Playlist{ID: "PLIcq6QAlz4MSiXPMMV9q3Itd0lChE3f-T", Name: "Arnold Schwarzenegger"})
	//if err != nil {
	//	log.Fatal(err)
	//}
	//
	//err = annotation.LoadS3AnnotationsStream("/tmp", "eu-central-1", "composey-media", "hls/TheBigBangTheory", c,  annotation.Playlist{ID: "tbbt", Name: "The Big Bang Theory"})
	//if err != nil {
	//	log.Fatal(err)
	//}

	err = c.Database().Client().Disconnect(context.Background())

	if err != nil {
		log.Fatal(err)
	}
}
