package annotation

import (
	"context"
	"github.com/asticode/go-astisub"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestMain(m *testing.M) {

	os.Exit(m.Run())
}

func TestCreateSub(t *testing.T) {
	subs, err := astisub.OpenFile(filepath.Clean("testData/testSub.srt"))

	assert.Equal(t, nil, err)

	start := time.Second * 270
	end := time.Second * 330

	newSub := CreateSub(*subs, start, end)

	assert.Equal(t, 22, len(newSub.Items))

	for _, sub := range newSub.Items {
		assert.Equal(t, true, sub.StartAt >= 0)
		assert.Equal(t, true, sub.EndAt > 0)
		assert.Equal(t, true, sub.StartAt < end-start)
		assert.Equal(t, true, sub.EndAt <= end-start)
	}
}

func TestCreateSubFile(t *testing.T) {
	subs, err := astisub.OpenFile(filepath.Clean("testData/testSub.srt"))
	assert.Equal(t, nil, err)

	start := time.Second * 720
	end := time.Second * 780

	newSub := CreateSub(*subs, start, end)

	path := "/tmp/thanks.srt"
	f, err := os.Create(path)
	assert.Equal(t, nil, err)

	err = newSub.WriteToSRT(f)
	assert.Equal(t, nil, err)

	err = os.Remove(path)
	assert.Equal(t, nil, err)

}

func TestFindLocalSubs(t *testing.T) {
	annotations, err := FindLocalSubs("testData", Playlist{"testData", "testData"})
	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(annotations) > 0)
}

func TestConnectMongoAtlasCollection(t *testing.T) {

	c, err := ConnectMongoDB(os.Getenv("MONGO_ATLAS_HOST"))
	assert.Equal(t, nil, err)

	dbs, err := c.ListDatabaseNames(context.Background(), bson.D{})
	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(dbs) > 0)

	c.Disconnect(context.Background())
}

func TestConnectMongoLocalCollection(t *testing.T) {

	client, err := ConnectMongoDB(os.Getenv("MONGO_HOST"))
	assert.Equal(t, nil, err)

	client.Disconnect(context.Background())
}

func TestLoadSubsToMongoDBLocal(t *testing.T) {
	annotations, err := FindLocalSubs("testData", Playlist{"testData", "testData"})
	assert.Equal(t, nil, err)
	c, err := ConnectMongoDBCollection(os.Getenv("MONGO_HOST"), "test", "test")
	assert.Equal(t, nil, err)

	assert.Equal(t, nil, UploadToDB(annotations, c))

	result := c.FindOne(context.Background(), bson.D{{"subtitle.text", "If it's unobserved, it will."}})
	assert.Equal(t, nil, result.Err())

	a := &Annotation{}
	err = result.Decode(a)
	assert.Equal(t, "If it's unobserved, it will.", a.Subtitle.Text)
	assert.Equal(t, true, a.Subtitle.Segments == nil)
	assert.Equal(t, nil, err)

	err = c.Database().Client().Disconnect(context.Background())
	assert.Equal(t, nil, err)
}

func TestLoadSubsToMongoDBRemote(t *testing.T) {

	annotations, err := FindLocalSubs("testData", Playlist{"testData", "testData"})
	assert.Equal(t, nil, err)
	c, err := ConnectMongoDBCollection(os.Getenv("MONGO_ATLAS_HOST"), "test", "test")
	c.Drop(context.Background())
	assert.Equal(t, nil, err)
	assert.Equal(t, nil, UploadToDB(annotations, c))
	num, err := c.CountDocuments(context.Background(), bson.D{})
	assert.Equal(t, nil, err)
	assert.Equal(t, int64(1387), num)
	c.Drop(context.Background())
}

func TestSubtitle_FindInSegments(t *testing.T) {

	s := Subtitle{
		Segments: []TimedText{
			{
				Text: "hello",
			},
			{
				Text: "my",
			},
			{
				Text: "name",
			},
			{
				Text: "is",
			},
			{
				Text: "dude",
			},
		},
	}

	startIdx, endIdx, err := s.FindInSegments("my name is")
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, startIdx)
	assert.Equal(t, 3, endIdx)

	startIdx, endIdx, err = s.FindInSegments("dude")
	assert.Equal(t, nil, err)
	assert.Equal(t, 4, startIdx)
	assert.Equal(t, 4, endIdx)

	startIdx, endIdx, err = s.FindInSegments("hello")
	assert.Equal(t, nil, err)
	assert.Equal(t, 0, startIdx)
	assert.Equal(t, 0, endIdx)

	startIdx, endIdx, err = s.FindInSegments("my is")
	assert.Error(t, err)

	startIdx, endIdx, err = s.FindInSegments("fdfafd")
	assert.Error(t, err)

	s = Subtitle{
		Text: "my name is dude",
	}

	startIdx, endIdx, err = s.FindInSegments("my name is")
	assert.Error(t, err)

}
