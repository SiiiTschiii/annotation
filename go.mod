module gitlab.ethz.ch/chgerber/annotation/v2

go 1.12

require (
	github.com/asticode/go-astisub v0.0.0-20190514140258-c0ed7925c393
	github.com/asticode/go-astitools v1.1.0 // indirect
	github.com/asticode/go-astits v1.0.0 // indirect
	github.com/aws/aws-sdk-go v1.20.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mackerelio/go-osstat v0.0.0-20190412014440-b90c0b34edef // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.ethz.ch/chgerber/monitor v0.0.0-20190527191251-2bb9dd731340 // indirect
	gitlab.ethz.ch/chgerber/s3helper v0.0.0-20190527212420-8196b9b9a04f
	go.mongodb.org/mongo-driver v1.0.3
	golang.org/x/crypto v0.0.0-20190909091759-094676da4a83 // indirect
	golang.org/x/net v0.0.0-20190909003024-a7b16738d86b // indirect
	golang.org/x/sys v0.0.0-20190909082730-f460065e899a // indirect
	golang.org/x/tools v0.0.0-20190909030654-5b82db07426d // indirect
)
